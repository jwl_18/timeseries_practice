import numpy as np
import pandas as pd

def split_sequence(sequence, n_steps):
    X, y = [], []
    if len(sequence)<=n_steps:
        raise Exception("Invalid sequence and n_steps")
    for i in range(n_steps, len(sequence)):
        X.append(sequence[i-n_steps:i])
        y.append(sequence[i])
    return np.array(X), np.array(y)

if __name__=='__main__':
    series = np.array([i for i in range(1,11)])
    print(series.shape)
    X, y = split_sequence(series, 3)
    print(X.shape, y.shape)
    for i in range(X.shape[0]):
        print(X[i], y[i])
    X = X.reshape((X.shape[0],X.shape[1],1))
    print(X.shape)



