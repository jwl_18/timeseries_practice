import numpy as np
import pandas as pd
# step1:load data
# X = pd.read_csv('filename.csv')
n = 5000
data = [[i, i*10] for i in range(n)]
data = np.array(data)
print(data[:5,:])
print(data.shape)
# drop the time column
data = data[:,1]
print(data.shape)
# split into subsamples with length 200
length = 200
samples = []
y = []
for i in range(0, n, length):
    if i+length<n:
        samples.append(data[i:i+length])
        y.append(data[i+length])
print(len(samples))
samples = np.array(samples)
y = np.array(y)
samples = samples.reshape((samples.shape[0], samples.shape[1], 1))
print(samples.shape)