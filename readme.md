This repos is for practices of "Deep learning for time series"

Chapter6: compose sequences

Chapter7: MLP practices

Chapter8: CNN practices

Chapter9: LSTM practices

Chapter11: Simple methods for univariate forecast

Chapter12: ETS model for univariate forecast

Chapter13: ARX model for univariate forecast

Chapter14: DeepLearning methods for univariate forecast