from numpy import mean, median, array
import pandas as pd
import matplotlib.pyplot as plt
from math import sqrt
from multiprocessing import cpu_count
from joblib import Parallel, delayed
from warnings import catch_warnings, filterwarnings
from sklearn.metrics import mean_squared_error
from statsmodels. tsa. statespace.sarimax import SARIMAX


# one-step Holt Winter's Exponential Smoothing forecast
def sarima_forecast(history, config):
    order, sorder, trend = config
    model = SARIMAX(history, order=order, seasonal_order=sorder, trend=trend, \
    enforce_stationarity=False, enforce_invertibility=False)
    model_fit = model.fit(disp=False)
    yhat = model_fit.predict(len(history), len(history))
    return yhat[0]

def walk_forward_validation(data, n_test, cfg):
    predictions = []
    train, test = train_test_split(data, n_test)
    history = [x for x in train]
    for i in range(n_test):
        yhat = sarima_forecast(history, cfg)
        predictions.append(yhat)
        history.append(test[i])
    error = measure_rmse(test, predictions)
    return error

def sarima_configs(seasonal=[0]):
    cfgs = []
    # define config lists
    p_params = [0,1,2]
    d_params = [0,1]
    q_params = [0,1,2]
    t_params = ['n','c','t','ct']
    P_params = [0,1,2]
    D_params = [0,1]
    Q_params = [0,1,2]
    m_params = seasonal
    # create config instance
    for p in p_params:
        for d in d_params:
            for q in q_params:
                for t in t_params:
                    for P in P_params:
                        for D in D_params:
                            for Q in Q_params:
                                for m in m_params:
                                    cfgs.append([(p,d,q),(P,D,Q,m),t])
    return cfgs

def score_model(data, n_test, cfg, debug=False):
    result=None
    # convert config to a key
    key = str(cfg)
    if debug:
        result = walk_forward_validation(data, n_test, cfg)
    else:
        try:
            with catch_warnings():
                filterwarnings("ignore")
                result = walk_forward_validation(data, n_test, cfg)
        except:
            result = None
    # if check for an interesting result
    if result is not None:
        print('> Model[%s %.3f' %(key, result))
    return (key, result)

def grid_search(data, cfg_list, n_test, parallel=True):
    scores = None
    if parallel:
        # execute configs in parallel
        executor = Parallel(n_jobs=cpu_count(), backend='multiprocessing')
        tasks = (delayed(score_model)(data, n_test, cfg) for cfg in cfg_list)
        scores = executor(tasks)
    else:
        scores = [score_model(data, n_test, cfg) for cfg in cfg_list]
    # remove empty results
    scores = [r for r in scores if r[1]!=None]
    # sort configs by error, asc
    scores.sort(key=lambda tup:tup[1])
    return scores

def train_test_split(data, n_test):
    return data[:-n_test], data[-n_test:]

def measure_rmse(real, predicted):
    return sqrt(mean_squared_error(real, predicted))

if __name__=='__main__':
    series = pd.read_csv('../daily-total-female-births.csv', header=0, index_col=0)
    data = series.values
    data = data[-100:]
    n_test = 12
    cfg_list = sarima_configs()
    scores = grid_search(data, cfg_list, n_test)
    print('done')
    for cfg, error in scores[:3]:
        print(cfg, error)
