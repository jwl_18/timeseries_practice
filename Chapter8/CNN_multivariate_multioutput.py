# multi input and multi output with multi head
import numpy as np
import pandas as pd
import tensorflow as tf

from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import Flatten
from tensorflow.keras.layers import Conv1D
from tensorflow.keras.layers import MaxPooling1D
from tensorflow.keras.layers import Concatenate
from tensorflow.keras.layers import Input
from tensorflow.keras.models import Model

def split_sequence(sequence, n_steps):
    X, y = [], []
    if len(sequence)<=n_steps:
        raise Exception("Invalid sequence and n_steps")
    for i in range(sequence.shape[0]):
        endix = i+n_steps
        if endix>=sequence.shape[0]:
            break
        X.append(sequence[i:endix, :])
        y.append(sequence[endix, :])
    return np.array(X), np.array(y)

if __name__=='__main__':
    # define multivariate input
    in_seq1 = np.array([10, 20, 30, 40, 50, 60, 70, 80, 90] )
    in_seq2 = np.array([15, 25, 35, 45, 55, 65, 75, 85, 95] )
    out_seq = np.array([in_seq1[i] +in_seq2[i] for i in range(len(in_seq1))])

    # combine seqs
    in_seq1 = in_seq1.reshape(len(in_seq1),1)
    in_seq2 = in_seq2.reshape(len(in_seq2),1)
    out_seq = out_seq.reshape(len(out_seq),1)
    seqs = np.hstack((in_seq1, in_seq2, out_seq))
    print(seqs.shape)

    # split sequence
    n_steps = 3
    X, y = split_sequence(seqs, n_steps)
    n_features = X.shape[2]

    # split y
    y1 = y[:,0].reshape((y.shape[0],1))
    y2 = y[:,1].reshape((y.shape[0],1))
    y3 = y[:,2].reshape((y.shape[0],1))

    # define model
    # share bottom
    visible = Input(shape=(n_steps, n_features))
    cnn = Conv1D(filters=64, kernel_size=2, activation='relu')(visible)
    cnn = MaxPooling1D(pool_size=2)(cnn)
    cnn = Flatten()(cnn)
    # multi head output
    output1 = Dense(1)(cnn)
    output2 = Dense(1)(cnn)
    output3 = Dense(1)(cnn)
    # compile
    model = Model(inputs=visible, outputs=[output1, output2, output3])
    model.compile(optimizer='adam', loss='mse')

    # fit model and predict
    model.fit(X, [y1,y2,y3], epochs=1000, verbose=0)
    x_input = np.array([[70,75,145],[80,85,165],[90,95,185]])
    x_input = x_input.reshape((1, n_steps, n_features))
    yhat = model.predict(x_input, verbose=0)
    print(yhat)


