# multi input and one output with multi head
import numpy as np
import pandas as pd
import tensorflow as tf

from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import Flatten
from tensorflow.keras.layers import Conv1D
from tensorflow.keras.layers import MaxPooling1D
from tensorflow.keras.layers import Concatenate
from tensorflow.keras.layers import Input
from tensorflow.keras.models import Model

def split_sequence(sequence, n_steps):
    X, y = [], []
    if len(sequence)<n_steps:
        raise Exception("Invalid sequence and n_steps")
    for i in range(sequence.shape[0]):
        endix = i+n_steps
        if endix>sequence.shape[0]:
            break
        X.append(sequence[i:endix, :-1])
        y.append(sequence[endix-1, -1])
    return np.array(X), np.array(y)

if __name__=='__main__':
    # define multivariate input
    in_seq1 = np.array([10, 20, 30, 40, 50, 60, 70, 80, 90] )
    in_seq2 = np.array([15, 25, 35, 45, 55, 65, 75, 85, 95] )
    out_seq = np.array([in_seq1[i] +in_seq2[i] for i in range(len(in_seq1))])

    # combine seqs
    in_seq1 = in_seq1.reshape(len(in_seq1),1)
    in_seq2 = in_seq2.reshape(len(in_seq2),1)
    out_seq = out_seq.reshape(len(out_seq),1)
    seqs = np.hstack((in_seq1, in_seq2, out_seq))
    print(seqs.shape)

    # split sequence
    n_steps = 3
    X, y = split_sequence(seqs, n_steps)

    # split X
    n_features = 1
    X1 = X[:,:,0].reshape((X.shape[0], X.shape[1], n_features))
    X2 = X[:,:,1].reshape((X.shape[0], X.shape[1], n_features))

    # define model
    # first head
    visible1 = Input(shape=(n_steps, n_features))
    cnn1 = Conv1D(filters=64, kernel_size=2, activation='relu')(visible1)
    cnn1 = MaxPooling1D(pool_size=2)(cnn1)
    cnn1 = Flatten()(cnn1)
    # second head
    visible2 = Input(shape=(n_steps, n_features))
    cnn2 = Conv1D(filters=64, kernel_size=2, activation='relu')(visible2)
    cnn2 = MaxPooling1D(pool_size=2)(cnn2)
    cnn2 = Flatten()(cnn2)
    # merge input models
    merge = Concatenate(axis=1)([cnn1, cnn2])
    dense = Dense(50, activation='relu')(merge)
    output = Dense(1)(dense)
    model = Model(inputs=[visible1, visible2], outputs=output)
    model.compile(optimizer='adam', loss='mse')

    # fit model and predict
    model.fit([X1, X2], y, epochs=1000, verbose=0)
    x_input = np.array([[80,85],[90,95],[100,105]])
    x1_input = x_input[:,0].reshape((1,n_steps, n_features))
    x2_input = x_input[:,1].reshape((1,n_steps, n_features))
    yhat = model.predict([x1_input, x2_input], verbose=0)
    print(yhat)


