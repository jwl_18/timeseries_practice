# univariate input and one output
import numpy as np
import pandas as pd
import tensorflow as tf

from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import Flatten
from tensorflow.keras.layers import Conv1D
from tensorflow.keras.layers import MaxPooling1D

def split_sequence(sequence, n_steps):
    X, y = [], []
    if len(sequence)<=n_steps:
        raise Exception("Invalid sequence and n_steps")
    for i in range(len(sequence)):
        endix = i+n_steps
        if endix>=len(sequence):
            break
        X.append(sequence[i:endix])
        y.append(sequence[endix])
    return np.array(X), np.array(y)


if __name__=='__main__':
    # load data
    raw_seq = [i*10 for i in range(1,10)]
    n_steps = 3
    X,y = split_sequence(raw_seq, n_steps)
    for i in range(len(X)):
        print(X[i], y[i])

    # reshape X
    n_features = 1
    X = X.reshape((X.shape[0], X.shape[1], n_features))

    # define model
    model = Sequential()
    model.add(Conv1D(filters=64, kernel_size=2, activation='relu', input_shape=(n_steps, n_features)))
    model.add(MaxPooling1D(pool_size=2))
    model.add(Flatten())
    model.add(Dense(50, activation='relu'))
    model.add(Dense(1))
    model.compile(optimizer='adam', loss='mse')
    model.fit(X, y, epochs=1000, verbose=0)

    # test
    x_input = np.array([70,80,90])
    x_input = x_input.reshape(1, n_steps, n_features)
    yhat = model.predict(x_input, verbose=0)
    print(yhat)

