# multivariate input one output with multihead
import numpy as np
import pandas as pd
import tensorflow as tf

# from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import Input
from tensorflow.keras.models import Model
from tensorflow.keras.layers import Concatenate

def split_sequence(sequence, n_steps):
    X, y = [], []
    if len(sequence)<n_steps:
        raise Exception("Invalid sequence and n_steps")
    for i in range(sequence.shape[0]):
        endix = i+n_steps
        if endix>sequence.shape[0]:
            break
        X.append(sequence[i:endix, :-1])
        y.append(sequence[endix-1, -1])
    return np.array(X), np.array(y)

if __name__=='__main__':
    # define multivariate input
    in_seq1 = np.array([10, 20, 30, 40, 50, 60, 70, 80, 90] )
    in_seq2 = np.array([15, 25, 35, 45, 55, 65, 75, 85, 95] )
    out_seq = np.array([in_seq1[i] +in_seq2[i] for i in range(len(in_seq1))])

    # combine seqs
    in_seq1 = in_seq1.reshape(len(in_seq1),1)
    in_seq2 = in_seq2.reshape(len(in_seq2),1)
    out_seq = out_seq.reshape(len(out_seq),1)
    seqs = np.hstack((in_seq1, in_seq2, out_seq))
    print(seqs.shape)

    # convert seqs into sequence
    n_steps = 3
    X, y = split_sequence(seqs, n_steps)
    for i in range(len(y)):
        print(X[i], y[i])
    
    X1 = X[:,:,0]
    X2 = X[:,:,1]
    


    # MLP model
    visible1 = Input(shape=(n_steps, ))
    dense1 = Dense(50, activation='relu')(visible1)

    visible2 = Input(shape=(n_steps, ))
    dense2 = Dense(50, activation='relu')(visible2)

    merge = Concatenate(axis=1)([dense1, dense2])
    output = Dense(1)(merge)
    model = Model(inputs=[visible1, visible2], outputs=output)

    model.compile(optimizer='adam', loss='mse')
    model.fit([X1,X2], y, epochs=2000, verbose=0)

    # predict
    x_input = np.array([[80,85],[90,95],[100,105]])
    x1_input = x_input[:,0].reshape((1,n_steps))
    x2_input = x_input[:,1].reshape((1,n_steps))
    yhat = model.predict([x1_input, x2_input], verbose=0)
    print(yhat)
