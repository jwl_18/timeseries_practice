# univariate input and onestep output
import numpy as np
import pandas as pd
import tensorflow as tf

from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense

def split_sequence(sequence, n_steps):
    X, y = [], []
    if len(sequence)<=n_steps:
        raise Exception("Invalid sequence and n_steps")
    for i in range(n_steps, len(sequence)):
        X.append(sequence[i-n_steps:i])
        y.append(sequence[i])
    return np.array(X), np.array(y)


if __name__=='__main__':
    # load data
    raw_seq = [i*10 for i in range(1,10)]
    n_steps = 3
    X,y = split_sequence(raw_seq, n_steps)

    # MLP
    model = Sequential()
    model.add(Dense(100, activation='relu', input_dim=n_steps))
    model.add(Dense(1))
    model.compile(optimizer='adam', loss='mse')
    model.fit(X, y, epochs=2000, verbose=0)

    # test
    x_input = np.array([70,80,90])
    x_input = x_input.reshape(1, n_steps)
    yhat = model.predict(x_input, verbose=0)
    print(yhat)

