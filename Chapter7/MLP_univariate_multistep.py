# univariate input and multistep output
import numpy as np
import pandas as pd
import tensorflow as tf

from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense

def split_sequence(sequence, n_steps_in, n_steps_out):
    X, y = [], []
    if len(sequence)<n_steps_in+n_steps_out:
        raise Exception("Invalid sequence and n_steps")
    for i in range(len(sequence)):
        endix = i+n_steps_in
        out_endix = endix+n_steps_out
        if out_endix>len(sequence):
            break
        X.append(sequence[i:endix])
        y.append(sequence[endix:out_endix])
    return np.array(X), np.array(y)


if __name__=='__main__':
    # load data
    raw_seq = [i*10 for i in range(1,10)]
    n_steps_in, n_steps_out = 3, 2
    X,y = split_sequence(raw_seq, n_steps_in, n_steps_out)

    # MLP
    model = Sequential()
    model.add(Dense(100, activation='relu', input_dim=n_steps_in))
    model.add(Dense(n_steps_out))
    model.compile(optimizer='adam', loss='mse')
    model.fit(X, y, epochs=2000, verbose=0)

    # test
    x_input = np.array([70,80,90])
    x_input = x_input.reshape(1, n_steps_in)
    yhat = model.predict(x_input, verbose=0)
    print(yhat)

