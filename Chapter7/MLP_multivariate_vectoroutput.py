# multi input and multi output but onestep with a vector
import numpy as np
import pandas as pd
import tensorflow as tf

from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import Input
from tensorflow.keras.models import Model
from tensorflow.keras.layers import Concatenate

def split_sequence(sequence, n_steps):
    X, y = [], []
    if len(sequence)<=n_steps:
        raise Exception("Invalid sequence and n_steps")
    for i in range(sequence.shape[0]):
        endix = i+n_steps
        if endix>=sequence.shape[0]:
            break
        X.append(sequence[i:endix, :])
        y.append(sequence[endix, :])
    return np.array(X), np.array(y)

if __name__=='__main__':
    # define multivariate input
    in_seq1 = np.array([10, 20, 30, 40, 50, 60, 70, 80, 90] )
    in_seq2 = np.array([15, 25, 35, 45, 55, 65, 75, 85, 95] )
    out_seq = np.array([in_seq1[i] +in_seq2[i] for i in range(len(in_seq1))])

    # combine seqs
    in_seq1 = in_seq1.reshape(len(in_seq1),1)
    in_seq2 = in_seq2.reshape(len(in_seq2),1)
    out_seq = out_seq.reshape(len(out_seq),1)
    seqs = np.hstack((in_seq1, in_seq2, out_seq))
    print(seqs.shape)

    # convert seqs into sequence
    n_steps = 3
    X, y = split_sequence(seqs, n_steps)
    for i in range(len(y)):
        print(X[i], y[i])
    
    # flatten input
    n_input = X.shape[1]*X.shape[2]
    X = X.reshape((X.shape[0], n_input))
    for i in range(len(y)):
        print(X[i], y[i])
    n_output = y.shape[1]

    # MLP model
    model = Sequential()
    model.add(Dense(100, activation='relu', input_dim=n_input))
    model.add(Dense(n_output))

    model.compile(optimizer='adam', loss='mse')
    model.fit(X, y, epochs=2000, verbose=0)

    # predict
    x_input = np.array([[70,75,145],[80,85,165],[90,95,185]])
    x_input = x_input.reshape((1,n_input))
    yhat = model.predict(x_input, verbose=0)
    print(yhat)
