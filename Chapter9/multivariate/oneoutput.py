# multi input and one output for one step
import numpy as np
import pandas as pd
import tensorflow as tf

from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import LSTM

def split_sequence(sequence, n_steps):
    X, y = [], []
    if len(sequence)<n_steps:
        raise Exception("Invalid sequence and n_steps")
    for i in range(sequence.shape[0]):
        endix = i+n_steps
        if endix>sequence.shape[0]:
            break
        X.append(sequence[i:endix, :-1])
        y.append(sequence[endix-1, -1])
    return np.array(X), np.array(y)

if __name__=='__main__':
    # define multivariate input
    in_seq1 = np.array([10, 20, 30, 40, 50, 60, 70, 80, 90] )
    in_seq2 = np.array([15, 25, 35, 45, 55, 65, 75, 85, 95] )
    out_seq = np.array([in_seq1[i] +in_seq2[i] for i in range(len(in_seq1))])

    # combine seqs
    in_seq1 = in_seq1.reshape(len(in_seq1),1)
    in_seq2 = in_seq2.reshape(len(in_seq2),1)
    out_seq = out_seq.reshape(len(out_seq),1)
    seqs = np.hstack((in_seq1, in_seq2, out_seq))
    print(seqs.shape)

    # split sequence
    n_steps = 3
    X, y = split_sequence(seqs, n_steps)
    n_features = X.shape[2]

    # define model
    model = Sequential()
    model.add(LSTM(50, activation='relu', input_shape=(n_steps, n_features)))
    model.add(Dense(1))
    model.compile(optimizer='adam', loss='mse')
    model.fit(X, y, epochs=1000, verbose=0)

    # test
    x_input = np.array([[80,85],[90,95],[100,105]])
    x_input = x_input.reshape(1, n_steps, n_features)
    yhat = model.predict(x_input, verbose=0)
    print(yhat)

