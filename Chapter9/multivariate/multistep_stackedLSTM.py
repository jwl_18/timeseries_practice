# multi input and multi step output without multi parallel
import numpy as np
import pandas as pd

from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import LSTM

def split_sequence(sequence, n_steps_in, n_steps_out):
    X, y = [], []
    if len(sequence)<n_steps_in+n_steps_out-1:
        raise Exception("Invalid sequence and n_steps")
    for i in range(sequence.shape[0]):
        endix = i+n_steps_in
        out_endix = endix+n_steps_out-1
        if out_endix>sequence.shape[0]:
            break
        X.append(sequence[i:endix, :-1])
        y.append(sequence[endix-1:out_endix, -1])
    return np.array(X), np.array(y)


if __name__=='__main__':
    # define multivariate input
    in_seq1 = np.array([10, 20, 30, 40, 50, 60, 70, 80, 90] )
    in_seq2 = np.array([15, 25, 35, 45, 55, 65, 75, 85, 95] )
    out_seq = np.array([in_seq1[i] +in_seq2[i] for i in range(len(in_seq1))])

    # combine seqs
    in_seq1 = in_seq1.reshape(len(in_seq1),1)
    in_seq2 = in_seq2.reshape(len(in_seq2),1)
    out_seq = out_seq.reshape(len(out_seq),1)
    seqs = np.hstack((in_seq1, in_seq2, out_seq))

    # convert seqs into sequence
    n_steps_in, n_steps_out = 3, 2
    X, y = split_sequence(seqs, n_steps_in, n_steps_out)
    n_features = X.shape[2]

    # define model, compile and fit
    model = Sequential()
    model.add(LSTM(100, activation='relu', return_sequences=True, input_shape=(n_steps_in, n_features)))
    model.add(LSTM(100, activation='relu'))
    model.add(Dense(n_steps_out))

    model.compile(optimizer='adam', loss='mse')
    model.fit(X, y, epochs=200, verbose=0)

    # predict
    x_input = np.array([[70,75],[80,85],[90,95]])
    x_input = x_input.reshape((1, n_steps_in, n_features))
    yhat = model.predict(x_input, verbose=0)
    print(yhat)

