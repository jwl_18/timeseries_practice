from numpy import mean, median
import pandas as pd
import matplotlib.pyplot as plt
from math import sqrt
from multiprocessing import cpu_count
from joblib import Parallel, delayed
from warnings import catch_warnings, filterwarnings
from sklearn.metrics import mean_squared_error

def simple_forecast(history, config):
    n, offset, avg_type = config
    if avg_type == 'persist':
        return history[-n]
    values = []
    if offset==1:
        values = history[-n:]
    else:
        # skip bad configs
        if n*offset>len(history):
            raise Exception('Config beyond end of data: %d %d' %(n,offset))
        for i in range(1,n+1):
            ix = i*offset
            values.append(history[-ix])
    # check if we can average
    if len(values)<2:
        raise Exception('Cannot calculate average')
    if avg_type=='mean':
        return (mean(values))
    return median(values)

def train_test_split(data, n_test):
    return data[:-n_test], data[-n_test:]

def measure_rmse(real, predicted):
    return sqrt(mean_squared_error(real, predicted))

def walk_forward_validation(data, n_test, cfg):
    predictions = []
    train, test = train_test_split(data, n_test)
    history = [x for x in train]
    for i in range(n_test):
        yhat = simple_forecast(history, cfg)
        predictions.append(yhat)
        history.append(test[i])
    error = measure_rmse(test, predictions)
    return error

def score_model(data, n_test, cfg, debug=False):
    result=None
    # convert config to a key
    key = str(cfg)
    if debug:
        result = walk_forward_validation(data, n_test, cfg)
    else:
        try:
            with catch_warnings():
                filterwarnings("ignore")
                result = walk_forward_validation(data, n_test, cfg)
        except:
            result = None
    # if check for an interesting result
    if result is not None:
        print('> Model[%s %.3f' %(key, result))
    return (key, result)

def grid_search(data, cfg_list, n_test, parallel=True):
    scores = None
    if parallel:
        # execute configs in parallel
        executor = Parallel(n_jobs=cpu_count(), backend='multiprocessing')
        tasks = (delayed(score_model)(data, n_test, cfg) for cfg in cfg_list)
        scores = executor(tasks)
    else:
        scores = [score_model(data, n_test, cfg) for cfg in cfg_list]
    # remove empty results
    scores = [r for r in scores if r[1]!=None]
    # sort configs by error, asc
    scores.sort(key=lambda tup:tup[1])
    return scores

def simple_configs(max_length, offsets=[1]):
    configs = list()
    for i in range(1, max_length+1):
        for o in offsets:
            for t in ['persist', 'mean', 'median']:
                cfg = [i,o,t]
                configs.append(cfg)
    return configs



if __name__=='__main__':
    series = pd.read_csv('../monthly-car-sales.csv', header=0, index_col=0)
    print(series.shape)
    # plot
    plt.plot(series)
    plt.xticks([])
    plt.savefig('monthly-car-sales.png')

    data = series.values
    n_test = 12
    max_length = len(data) - n_test
    cfg_list = simple_configs(max_length, offsets=[1,6,12])
    scores = grid_search(data, cfg_list, n_test, parallel=False)
    print('done')
    for cfg, error in scores[:3]:
        print(cfg, error)
    

