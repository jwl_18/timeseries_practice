from numpy import mean, median, array
import pandas as pd
import matplotlib.pyplot as plt
from math import sqrt
from multiprocessing import cpu_count
from joblib import Parallel, delayed
from warnings import catch_warnings, filterwarnings
from sklearn.metrics import mean_squared_error

from statsmodels.tsa.holtwinters import ExponentialSmoothing


# one-step Holt Winter's Exponential Smoothing forecast
def exp_smoothing_forecast(history, config):
    t, d, s, p, b, r = config
    history = array(history)
    model = ExponentialSmoothing(history, trend=t, damped=d, seasonal=s, seasonal_periods=p)
    model_fit = model.fit(optimized=True, use_boxcox=b, remove_bias=r)
    yhat = model_fit.predict(len(history), len(history))
    return yhat[0]

def walk_forward_validation(data, n_test, cfg):
    predictions = []
    train, test = train_test_split(data, n_test)
    history = [x for x in train]
    for i in range(n_test):
        yhat = exp_smoothing_forecast(history, cfg)
        predictions.append(yhat)
        history.append(test[i])
    error = measure_rmse(test, predictions)
    return error

def exp_smoothing_configs(seasonal=[None]):
    cfgs = []
    # define config lists
    t_params = ['add', 'mul', None]
    d_params = [True, False]
    s_params = ['add', 'mul', None]
    p_params = seasonal
    b_params = [True, False]
    r_params = [True, False]
    # create config instance
    for t in t_params:
        for d in d_params:
            for s in s_params:
                for p in p_params:
                    for b in b_params:
                        for r in r_params:
                            cfgs.append([t,d,s,p,b,r])
    return cfgs

def score_model(data, n_test, cfg, debug=False):
    result=None
    # convert config to a key
    key = str(cfg)
    if debug:
        result = walk_forward_validation(data, n_test, cfg)
    else:
        try:
            with catch_warnings():
                filterwarnings("ignore")
                result = walk_forward_validation(data, n_test, cfg)
        except:
            result = None
    # if check for an interesting result
    if result is not None:
        print('> Model[%s %.3f' %(key, result))
    return (key, result)

def grid_search(data, cfg_list, n_test, parallel=True):
    scores = None
    if parallel:
        # execute configs in parallel
        executor = Parallel(n_jobs=cpu_count(), backend='multiprocessing')
        tasks = (delayed(score_model)(data, n_test, cfg) for cfg in cfg_list)
        scores = executor(tasks)
    else:
        scores = [score_model(data, n_test, cfg) for cfg in cfg_list]
    # remove empty results
    scores = [r for r in scores if r[1]!=None]
    # sort configs by error, asc
    scores.sort(key=lambda tup:tup[1])
    return scores

def train_test_split(data, n_test):
    return data[:-n_test], data[-n_test:]

def measure_rmse(real, predicted):
    return sqrt(mean_squared_error(real, predicted))

if __name__=='__main__':
    series = pd.read_csv('../daily-total-female-births.csv', header=0, index_col=0)
    data = series.values
    n_test = 165
    cfg_list = exp_smoothing_configs()
    scores = grid_search(data, cfg_list, n_test)
    print('done')
    for cfg, error in scores[:3]:
        print(cfg, error)
